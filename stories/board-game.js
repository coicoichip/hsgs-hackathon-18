import React from "react";
import { storiesOf } from "@storybook/react";
import BoardTOH from "../example/index.jsx";
import BoardSDK from "../sudoku/index.jsx";
import GameTOH from "../example/lib/tower_of_hanoi.js";
import GameSDK from "../sudoku/lib/sudoku.js";
import ReactGame from "react-gameboard/lib/component";

const TOH = ReactGame(GameTOH);
const SDK = ReactGame(GameSDK);

storiesOf("Sudoku" , module) 
  .add ("9*9", () => (
    <SDK height = {3}>
      <BoardSDK/>
    </SDK>
  ));
