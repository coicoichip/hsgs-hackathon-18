import { injectGlobal } from "styled-components";

/* eslint no-unused-expressions: 0 */
injectGlobal`
  @font-face {
    font-family: 'Bodoni Ornaments';
    src: url('./Bodoni Ornaments.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Bodoni Ornaments', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Bodoni Ornaments', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;
