import React from "react";
import Sudoku from "./lib/sudoku.js";

import "./index.css";

class Row extends React.Component {
  render() {
    const array = [];
    for (let id = 8; id >= 0; --id) {
      if (id < this.props.pile.length) {
        array.push(<td key={id}> <input/> </td>);
      } else {
        array.push(<td key={id}> <input/> </td>);
      }
    }
    console.log(array);
    return <tr>{array}</tr>;
  }
}

class BoardSDK extends React.Component {
  constructor(props) {
    super(props);
    let a = this.props.state.piles;
    this.state = { value: a };
    this.handleChange = this.handleChange.bind(this);
  }
  change(event) {
    console.log(event);
  }
  handleChange(event) {
    let a = this.state.value;
    console.log(event.target.id);
    a[ parseInt(event.target.id / 10)][ parseInt(event.target.id % 10)][0] = event.target.value;
    this.setState({ value: a });
    console.log(this.props.state.piles);
    this.props.onchange({ x: parseInt(event.target.id / 10), y:  parseInt(event.target.id % 10), n: [event.target.value ,a[ parseInt(event.target.id / 10)][ parseInt(event.target.id % 10)][1]] });
  }

  render() {
    // Calculate the value of N
    // let N = 0;
    // for (let i = 0; i < 9; ++i) {
    //   for (let j of this.props.state.piles[i]) {
    //     N = Math.max(N, j);
    //   }
    // }
    // const moves = [];
    // for (let i = 1; i <= 9; ++i)
    //   for (let j = 1; j <= 9; ++j) {
    //     moves.push(
    //       <div>
    //         <button onClick={() => this.props.move({ x: i, y: j })}>
    //           Move {i} => {j}
    //         </button>
    //       </div>
    //     );
    //   }
    // const err = this.props.error ? this.props.error.message : null;
    const array = [];
    for (var i = 0; i < 9; ++i) {
      const subarray = [];
      for (var k = 0; k < 9; ++k) {
        if (k % 3 == 2) {
          //console.log(this.props);
          if (
            this.props.state.piles[i][k][1] == false
          )
            subarray.push(
              <input
                type="text"
                value={this.state.value[i][k][0] == 0 && this.state.value[i][k][1] == false ? "" : this.state.value[i][k][0]}
                id = {i*10 + k}
                style={{
                  borderRadius: "5px",
                  borderWidth: "1px",
                  borderStyle: "solid",
                  borderColor: "#3fa4db",
                  height: "50px",
                  width: "50px",
                  fontSize: "25px",
                  textAlign: "center",
                  marginRight: "30px",
                  color: "#167fb7"
                }}
                
                type="text"
                onChange={this.handleChange}
              />
            );
          else
            subarray.push(
              <button
                style={{
                  borderRadius: "5px",
                  borderWidth: "1px",
                  borderStyle: "solid",
                  borderColor: "#3fa4db",
                  height: "53px",
                  width: "53px",
                  fontSize: "25px",
                  textAlign: "center",
                  marginRight: "30px"
                }}
                id = {i*10 + k}
              >
                {" "}
                {this.props.state.piles[i][k][0]}{" "}
              </button>
            );
        } else {
          if (
            this.props.state.piles[i][k][1] == false
          )
            subarray.push(
              <input
                type="text"
                value={this.state.value[i][k][0] == 0 && this.state.value[i][k][1] == false ? "" : this.state.value[i][k][0]}
                id = {i*10 + k}
                style={{                
                  borderRadius: "5px",
                  borderWidth: "1px",
                  borderStyle: "solid",
                  borderColor: "#3fa4db",
                  height: "50px",
                  width: "50px",
                  fontSize: "25px",
                  textAlign: "center",
                  marginRight: "10px",
                  color: "#167fb7"
                }}
                onChange={this.handleChange}
              />
            );
          else
            subarray.push(
              <button
                style={{
                  borderRadius: "5px",
                  borderWidth: "1px",
                  borderStyle: "solid",
                  borderColor: "#3fa4db",
                  height: "53px",
                  width: "53px",
                  fontSize: "25px",
                  textAlign: "center",
                  marginRight: "10px"
                }}
              >
                {" "}
                {this.props.state.piles[i][k][0]}{" "}
              </button>
            );
        }
      }
      if (i == 0) array.push(<div>{subarray}</div>);
      else {
        if (i % 3 == 1 || i % 3 == 2)
          array.push(<div style={{ marginTop: "10px" }}>{subarray} </div>);
        else if (i % 3 == 0)
          array.push(<div style={{ marginTop: "30px" }}>{subarray} </div>);
      }
    }

    return (
      <div className="container">
        <div className="picture-of-name" slyle={{ float: "left" }}>
          <img
            src={require("./lib/1.png")}
            style={{ width: "300px", height: "130px" }}
          />
          <h1
            style={{
              fontFamily: "sans-serif",
              color: "#3fa4db",
              marginLeft: "150px"
            }}
          >
            SUDOKU
          </h1>
        </div>
        <div
          style={{ float: "right", marginTop: "-100px", marginRight: "300px" }}
        >
          {array}
        </div>
        {/* <table style={{ border: "1px solid black" }}>
          <tbody>
            <Row N={N} pile={this.props.state.piles[0]} />
            <Row N={N} pile={this.props.state.piles[1]} />
            <Row N={N} pile={this.props.state.piles[2]} />
            <Row N={N} pile={this.props.state.piles[3]} />
            <Row N={N} pile={this.props.state.piles[4]} />
            <Row N={N} pile={this.props.state.piles[5]} />
            <Row N={N} pile={this.props.state.piles[6]} />
            <Row N={N} pile={this.props.state.piles[7]} />
            <Row N={N} pile={this.props.state.piles[8]} />
          </tbody>
        </table>
        <hr /> */}
        {/* <div className = "main-font"> thanh </div> */}
        {/* {moves} */}
        {/* <pre>{JSON.stringify(this.props)}</pre>
        <pre>{JSON.stringify(err)}</pre> */}
      </div>
    );
  }
}

export default BoardSDK;
